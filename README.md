# kochin_auto_flower_watering

![orchids](docs/img/IMG_20190427_122656.jpg)

The person I love so much gave me some orchids to take care. However
sometimes I need to go to foreign countries for for conferences or meetings
for few weeks. And the products I found cannot satisfy me. I want a clock
rather than just a timer. For example, I want to water my flower at 9AM
each day. If the product is timer based I can only start it on 9AM and set
the timer to 24 hours. Not very convenient if you want to set 5 machines
all at 9AM. Also some machine uses 5V pump and it is not that powerful to
water the flowers hanging high at around 1 meter from the ground.
Thus I decide to make it myself. And for orchids it is not very good to water
it everyday in Taiwan because that would make them drown. So it is better to
be programmable so that we can water the flower based on both time and the soil
moisture. But for the public source code we only provides the common code.
Watering by time or watering by soil moisture. If you need any complex
combinations of time and soil moisture please modify or branch this project
for yourself.

## Contents in this project

We split different sub-components to sub-directories. As following:

* arduino/ the software (sketch) that runs on Arduino UNO.
* hardware/ the hardware need to buy or need to print.

