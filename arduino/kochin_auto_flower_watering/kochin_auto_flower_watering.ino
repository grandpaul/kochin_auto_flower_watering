/*
    Arduino Auto Watering.
 Copyright (C) 2018 Ying-Chun Liu (PaulLiu) <paulliu@debian.org>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>

#include <Wire.h>
#include <EEPROM.h>
#include <Ds1302.h>
#include <LiquidCrystal_I2C.h>
#include <avr/wdt.h>

/* Comment the below line out to not have this feature and saves for binary size */
//#define CFG_ENABLE_MEMORYFREE 1

#ifdef CFG_ENABLE_MEMORYFREE
#include <MemoryFree.h>
#endif /* CFG_ENABLE_MEMORYFREE */

/* Soil Humid Sensor at A0 */
const int GPIO_SOILHUMID=0;
/* pump PWM pin at D9 */
const int GPIO_PUMP=9;
/* LCD backlight PWM at D10 */
const int GPIO_LCDBACKLIGHT=10;
/* LED indicate that pump is pumping at D13 */
const int GPIO_LED=13;
/* Pump max Voltage. Set this value correctly to NOT BURNING your pump */
const int PUMP_MAX_V = 12;
/* DS1302 RTC */
Ds1302 rtc (2,3,4);
/* LCD at I2C address 0x27, 16 columns, 2 rows */
LiquidCrystal_I2C lcd(0x27,16,2);  

/* working mode. 1: clock mode, 2: humid mode. 3: C+H mode. */
int workingMode = 0;

int lockMode = 0;

/* watering on HH:MM on each colockDD days */
int clockHH = 9;
int clockMM = 0;
int clockDD = 1;

/* dry level to water */
int humidTriggerLevel = 1;

/* watering speed for pumping */
int waterSpeedLevel = 1;

/* LCD backlight level */
int lcdBacklightLevel = 3;

/* how many seconds to water */
int waterDurationSec=3;

/* is pump currently pumping */
int isPumping = 0;

/* are we testing the pump? */
int isTestingPump = 0;

#define FPSTR001( pgm_ptr ) ( reinterpret_cast< const __FlashStringHelper * >( pgm_ptr ) )

/**
 * Config data for serial console
 * name - the name of the config.
 * type - 0 means integer
 * data - the address of global variables
 * eepromAddr - the address of EEPROM
 * minValue - the minimum value. Should between 0 ~ maxValue
 * maxValue - the maximum value. Should between minValue ~ 255
 * defaultValue - default value. Should between minValue ~ maxValue
 */
struct configDataType {
  const char *name;
  const int type;
  void * const data;
  const int eepromAddr;
  const int minValue;
  const int maxValue;
  const int defaultValue;
};

static struct configDataType configData_i = {
  .name = NULL,
  .type = 0,
  .data = NULL,
  .eepromAddr = 0,
  .minValue = -1,
  .maxValue = -1,
  .defaultValue = 0
};

/**
 * configData describes how many variable is used as a configuration
 */
const struct configDataType configData[] PROGMEM = {
  { /* index: 0 */
    .name = "clockDD",
    .type = 0,
    .data = (void *)(&clockDD),
    .eepromAddr=10,
    .minValue=1,
    .maxValue=30,
    .defaultValue=1
  }
  ,
  { /* index: 1 */
    .name = "clockHH",
    .type = 0,
    .data = (void *)(&clockHH),
    .eepromAddr=11,
    .minValue=0,
    .maxValue=23,
    .defaultValue=9
  }
  ,
  { /* index: 2 */
    .name = "clockMM",
    .type = 0,
    .data = (void *)(&clockMM),
    .eepromAddr=12,
    .minValue=0,
    .maxValue=59,
    .defaultValue=30
  }
  ,
  { /* index: 3 */
    .name = "humidTriggerLevel",
    .type = 0,
    .data = (void *)(&humidTriggerLevel),
    .eepromAddr=13,
    .minValue=1,
    .maxValue=3,
    .defaultValue=1
  }
  ,
  { /* index: 4 */
    .name = "workingMode",
    .type = 0,
    .data = (void *)(&workingMode),
    .eepromAddr=14,
    .minValue=0,
    .maxValue=3,
    .defaultValue=0
  }
  ,
  { /* index: 5 */
    .name = "lockMode",
    .type = 0,
    .data = (void *)(&lockMode),
    .eepromAddr=15,
    .minValue=0,
    .maxValue=1,
    .defaultValue=0
  }
  ,
  { /* index: 6 */
    .name = "lcdBacklightLevel",
    .type = 0,
    .data = (void *)(&lcdBacklightLevel),
    .eepromAddr=16,
    .minValue=1,
    .maxValue=4,
    .defaultValue=3
  }
  ,
  { /* index: 7 */
    .name = "waterDurationSec",
    .type = 0,
    .data = (void *)(&waterDurationSec),
    .eepromAddr=17,
    .minValue=0,
    .maxValue=254,
    .defaultValue=10
  }
  ,
  { /* index: 8 */
    .name = "waterSpeedLevel",
    .type = 0,
    .data = (void *)(&waterSpeedLevel),
    .eepromAddr=18,
    .minValue=1,
    .maxValue=PUMP_MAX_V,
    .defaultValue=3
  }
};

/**
 * Add 1 to a config variable
 * @param index the index of the configData
 */
void configDataAddOne(int index) {
  if (0 <= index && index < sizeof(configData)/sizeof(configData[0])) {
    memcpy_P(&configData_i, &(configData[index]),
             sizeof(struct configDataType));
    if (configData_i.type == 0) {
      if (configData_i.minValue == -1 && configData_i.maxValue == -1) {
        *((int *)(configData_i.data)) = *((int *)(configData_i.data))+1;
      } 
      else {
        int n = configData_i.maxValue - configData_i.minValue + 1;
        *((int *)(configData_i.data)) = (((*((int *)(configData_i.data)))-configData_i.minValue + 1) % n) + configData_i.minValue;
      }
    }
  }
}

/**
 * Decrease 1 to a config variable
 * @param index the index of configData
 */
void configDataSubtractOne(int index) {
  if (0 <= index && index < sizeof(configData)/sizeof(configData[0])) {
    memcpy_P(&configData_i, &(configData[index]),
             sizeof(struct configDataType));
    if (configData_i.type == 0) {
      if (configData_i.minValue == -1 && configData_i.maxValue == -1) {
        *((int *)(configData_i.data)) = *((int *)(configData_i.data))-1;
      } 
      else {
        int n = configData_i.maxValue - configData_i.minValue + 1;
        *((int *)(configData_i.data)) = (((*((int *)(configData_i.data)))-configData_i.minValue +n-1) % n) + configData_i.minValue;
      }
    }
  }
}

void setup() {
  lcd.init();
  lcd.backlight();  // enable backlight of LCD

  rtc.init();
  //rtc.halt(false); // set RTC to run-mode
  //rtc.writeProtect(false); // disable the write protection of RTC

  /* buttons */
  for (int i=5; i<9; i++) {
    pinMode(i, INPUT_PULLUP);
  }

  pinMode(GPIO_LCDBACKLIGHT, OUTPUT);
  pinMode(GPIO_PUMP, OUTPUT);
  pinMode(GPIO_LED, OUTPUT);

  Serial.begin(9600); // Serial console init
  while(!Serial); //Wait until Serial is ready before printing

  /* load config from EEPROM */
  for (int i=0; i<(sizeof(configData)/sizeof(configData[0])); i++) {
    memcpy_P(&configData_i, &(configData[i]),
             sizeof(struct configDataType));
    if (configData_i.type == 0) {
      int data;
      data = EEPROM.read(configData_i.eepromAddr);
      /* minValue == -1 AND maxValue == -1 means that we use raw value from
         EEPROM without sanity check */
      if (configData_i.minValue == -1 && configData_i.maxValue == -1) {
        *((int *)(configData_i.data)) = data;
      } 
      else {
        /* We need to check min and max. */
        if (configData_i.minValue <= data && data <= configData_i.maxValue) {
	  /* data is between min and max, we use data as config value */
          *((int *)(configData_i.data)) = data;
        } 
        else {
	  /* data is out ranged of min~max */
	  if (configData_i.minValue <= configData_i.defaultValue && configData_i.defaultValue <= configData_i.maxValue) {
	    /* default value is in min~max, we use default value */
            *((int *)(configData_i.data)) = configData_i.defaultValue;
	  } else {
	    /* default value is out ranged. We set the default value as (min+max)/2 */
            *((int *)(configData_i.data)) = (configData_i.minValue+configData_i.maxValue)/2;
	  }
        }
      }
    }
  }
  wdt_enable(WDTO_8S); /* watchdog will be unhappy if no one pets in 8 sec */
}

/**
 * keyState
 * sync with key stats. 1 means pushed. 0 means released
 */
byte keyState[4] = {
  0,0,0,0};

/**
 * The queue for key.
 */
byte keyQueue[4];
int keyQueueHead=0;
int keyQueueTail=0;
/* record the time when the key pressed (in ms) */
unsigned long keyPressedTime=0;

/**
 * Add a byte to the key queue
 *
 * @param a the byte that represent the key. Should be pin number.
 * @return 0 means queue full and the element is not add to the queue
 */
int keyQueueAdd(byte a) {
  if (  (keyQueueHead+1)%(sizeof(keyQueue)/sizeof(keyQueue[0])) == keyQueueTail ) {
    /* queue full */
    return 0;
  }
  keyQueue[keyQueueHead] = a;
  keyQueueHead = (keyQueueHead+1) %(sizeof(keyQueue)/sizeof(keyQueue[0]));
  return 1;
}

/**
 * test if the queue is empty
 * @return true - queue is empty. false - queue is not empty.
 */
int keyQueueIsEmpty() {
  if (keyQueueHead == keyQueueTail) {
    return 1;
  }
  return 0;
}

/**
 * Remove a byte from the queue and return the byte
 * @return the byte of the head of the queue
 */
byte keyQueuePoll() {
  byte ret = keyQueue[keyQueueTail];
  if (keyQueueIsEmpty()) {
    return 0;
  }
  keyQueueTail = (keyQueueTail+1) % (sizeof(keyQueue)/sizeof(keyQueue[0]));
  return ret;
}

/**
 * Scan the keyboard and update the keyState and keyQueue
 * @return always 0.
 */
int keyScan() {
  for (int i=5; i<9; i++) {
    int s;
    s = digitalRead(i);
    if (s == LOW) {
      /* key Pressed */
      if (keyState[i-5] < 20) {
        keyState[i-5]++;
      } 
      else {
        /* long press */
        keyQueueAdd(i);
      }
      keyPressedTime=millis();
    } 
    else {
      /* key not pressed */
      if (keyState[i-5] != 0) {
        /* key relased */
        keyQueueAdd(i);
      }
      keyState[i-5] = 0;
    }
  }
  return 0;
}

/**
 * Execute a command from Serial console
 * @param cmd the command comes from the Serial port
 */
void runCommand(char *cmd) {
  if (strncmp_P(cmd, PSTR("printenv"), 8)==0) {
    for (int i=0; i<sizeof(configData)/sizeof(configData[0]); i++) {
      memcpy_P(&configData_i, &(configData[i]),
               sizeof(struct configDataType));
      if (configData_i.type == 0) {
        /* type: int */
	Serial.print(configData_i.name);
	Serial.print('=');
        Serial.println(*((int *)(configData_i.data)));
      }
    }
  } 
  else if (strncmp_P(cmd, PSTR("help"), 4)==0) {
    Serial.println(F("printenv - print values of all environment variables"));
    Serial.println(F("setenv - set environment variable"));
    Serial.println(F("eeprom - eeprom operation"));
    Serial.println(F("rtc [MMDDhhmmYYYY.ss] - rtc operation"));
    Serial.println(F("pump - turn on/off pump"));
    Serial.println(F("soilhumid - soilhumid sensor"));
    Serial.println(F("free - print out free memory size"));
    Serial.println(F("version - show firmware version"));
  } 
  else if (strncmp_P(cmd, PSTR("setenv"), 6)==0) {
    char *saveptr1=NULL;
    char *cmd1 = strtok_rP(cmd, PSTR(" "), &saveptr1);
    char *var = strtok_rP(NULL, PSTR(" "), &saveptr1);
    if (var != NULL) {
      for (int i=0; i<sizeof(configData)/sizeof(configData[0]); i++) {
        memcpy_P(&configData_i, &(configData[i]),
                 sizeof(struct configDataType));
        if (strcmp(var, configData_i.name)==0) {
          if (configData_i.type == 0) {
            char *arg = strtok_rP(NULL, PSTR(" "), &saveptr1);
            if (arg != NULL) {
              int data=0;
              if (sscanf_P(arg, PSTR("%d"), &data) == 1) {
                int *p = (int *)(configData_i.data);
                *p = data;
                if (configData_i.eepromAddr != 0) {
                  EEPROM.write(configData_i.eepromAddr, data);
                }
              }
            }
          }
        }
      }
    }
  } 
  else if (strncmp_P(cmd, PSTR("eeprom"), 6)==0) {
    char *saveptr1=NULL;
    char *cmd1 = strtok_rP(cmd, PSTR(" "), &saveptr1);
    char *subcmd1 = strtok_rP(NULL, PSTR(" "), &saveptr1);
    if (subcmd1 == NULL) {
      Serial.println(F("eeprom dump - dump the data of EEPROM"));
      Serial.println(F("eeprom setb <addr(DEC)> <value(HEX)>"));
    } 
    else if (strcmp_P(subcmd1, PSTR("dump"))==0) {
      for (int i=0; i<512; i++) {
        int data;
        if (i%16 > 0) {
          Serial.print(' ');
        }
        if (i%16 == 8) {
          Serial.print(' ');
        }
	data = EEPROM.read(i);
	if (data <= 0x0f) {
	  Serial.print(0, HEX);
	}
	Serial.print(EEPROM.read(i), HEX);
        if (i%16 == 15) {
          Serial.println();
        }
      }
    } 
    else if (strcmp_P(subcmd1, PSTR("setb"))==0) {
      char *eAddr;
      eAddr = strtok_rP(NULL, PSTR(" "), &saveptr1);
      if (eAddr != NULL) {
        int eAddrI;
        if (sscanf_P(eAddr, PSTR("%d"), &eAddrI)==1) {
          char *hexValue;
          hexValue = strtok_rP(NULL, PSTR(" "), &saveptr1);
          if (hexValue != NULL) {
            int hexValueI;
            if (sscanf_P(hexValue, PSTR("%x"), &hexValueI)==1) {
              EEPROM.write(eAddrI, hexValueI);
            }
          }
        }
      }
    }
  } 
  else if (strncmp_P(cmd, PSTR("rtc"), 3)==0) {
    char *saveptr1=NULL;
    char *cmd1 = strtok_rP(cmd, PSTR(" "), &saveptr1);
    char *dateStr = strtok_rP(NULL, PSTR(" "), &saveptr1);
    if (dateStr == NULL) {
      Ds1302::DateTime t;
      rtc.getDateTime(&t);
      if (t.year < 1000) {
        Serial.print(0);
      }
      if (t.year < 100) {
        Serial.print(0);
      }
      if (t.year < 10) {
        Serial.print(0);
      }
      Serial.print(t.year);
      Serial.print('-');
      if (t.month < 10) {
        Serial.print(0);
      }
      Serial.print(t.month);
      Serial.print('-');
      if (t.day < 10) {
        Serial.print(0);
      }
      Serial.print(t.day);
      Serial.print(' ');
      if (t.hour < 10) {
        Serial.print(0);
      }
      Serial.print(t.hour);
      Serial.print(':');
      if (t.minute < 10) {
        Serial.print(0);
      }
      Serial.print(t.minute);
      Serial.print(':');
      if (t.second < 10) {
        Serial.print(0);
      }
      Serial.println(t.second);
    } 
    else if (strlen(dateStr) == 15) {
      Ds1302::DateTime t;
      t.month = (dateStr[0]-'0')*10+(dateStr[1]-'0');
      t.day = (dateStr[2]-'0')*10+(dateStr[3]-'0');
      t.hour = (dateStr[4]-'0')*10+(dateStr[5]-'0');
      t.minute = (dateStr[6]-'0')*10+(dateStr[7]-'0');
      t.year = ((dateStr[8]-'0')*1000+(dateStr[9]-'0')*100+(dateStr[10]-'0')*10+(dateStr[11]-'0')) % 100;
      t.second = (dateStr[13]-'0')*10+(dateStr[14]-'0');
      rtc.setDateTime(&t);
    }
  } 
  else if (strncmp_P(cmd, PSTR("pump"), 4)==0) {
    char *saveptr1=NULL;
    char *cmd1 = strtok_rP(cmd, PSTR(" "), &saveptr1);
    char *subcmd1 = strtok_rP(NULL, PSTR(" "), &saveptr1);
    if (subcmd1 == NULL) {
      Serial.println(F("pump on - turn on pump"));
      Serial.println(F("pump off - turn off pump"));
      if (isPumping) {
        Serial.println(F("pump is currently ON"));
      } 
      else {
        Serial.println(F("pump is currently OFF"));
      }
    } 
    else if (strcmp_P(subcmd1, PSTR("on"))==0) {
      isTestingPump=0;
      pumpStop();
      isTestingPump=1;
      pumpStart();
    } 
    else if (strcmp_P(subcmd1, PSTR("off")) == 0) {
      isTestingPump=0;
      pumpStop();
    }
  } 
  else if (strncmp_P(cmd, PSTR("soilhumid"), 5)==0) {
    int data = analogRead(GPIO_SOILHUMID);
    int dataLevel=0;
    for (int i=1; i<=3; i++) {
      if (data > getHumidTriggerByLevel(i)) {
        dataLevel = i;
      }
    }
    Serial.print(F("Humid: "));
    Serial.print(data);
    Serial.print(F(" (Level: "));
    Serial.print(dataLevel);
    Serial.println(')');
  } 
  else if (strncmp_P(cmd, PSTR("version"), 7)==0) {
    Serial.print(F("koching_auto_flower_watering ("));
    Serial.print(F(__DATE__));
    Serial.print(F(" - "));
    Serial.print(F(__TIME__));
    Serial.println(')');
  } 
  else if (strncmp_P(cmd, PSTR("free"), 4)==0) {
    Serial.print(F("Mem: "));
    #ifdef CFG_ENABLE_MEMORYFREE
    Serial.println(freeMemory());
    #else
    Serial.println(F("Not supported"));
    #endif
  } 
  else {
    Serial.print(F("Unknown command '"));
    Serial.print(cmd);
    Serial.println('\'');
  }
}

char serialBuf[40];
int serialBufLen=0;
/**
 * Handle the serial input.
 * This function will read any available bytes from the Serial console.
 * Put to the serialBuf and maintains the buffer.
 * And if it finds a carrier-return it will execute the command by calling
 * runCommand()
 */
void handleSerialCommand() {
  for (int i=0; Serial.available() > 0 && i <= 40; i++) {
    int incomingByte = 0;
    incomingByte = Serial.read();
    if (incomingByte == '\r') {
      /* run commands */
      Serial.println();
      runCommand(serialBuf);
      serialBufLen=0;
      serialBuf[0]='\0';
      Serial.print(F("kochin> "));
      Serial.flush();
    } 
    else if (incomingByte == '\n') {
      /* ignore */
    }
    else if (incomingByte == 127) {
      if (serialBufLen >= 1) {
        serialBufLen--;
        serialBuf[serialBufLen] = '\0';
        Serial.write(8);
        Serial.flush();
      }
    }
    else {
      if (serialBufLen+1 < (sizeof(serialBuf)/sizeof(serialBuf[0]))) {
        serialBuf[serialBufLen]=incomingByte;
        serialBufLen++;
        serialBuf[serialBufLen]='\0';
      }
      Serial.write(incomingByte);
      Serial.flush();
    }
  }
}

/**
 * get water speed by level.
 * We define 12 levels which represent the volts from 0~12.
 * @param waterSpeedLevel value 0~12, means volts that we need to give to
 *        the motor.
 * @return the PWM value for arduino to output.
 */
int getWaterSpeedByLevel(int waterSpeedLevel) {
  if (waterSpeedLevel > PUMP_MAX_V) {
    waterSpeedLevel = PUMP_MAX_V;
  }
  return (255/12) * waterSpeedLevel;
}

/**
 * start the pump
 */
int pumpStart() {
  if (! isPumping) {
    analogWrite(GPIO_PUMP, getWaterSpeedByLevel(waterSpeedLevel));
    digitalWrite(GPIO_LED, HIGH);
    isPumping = 1;
  }
  return 0;
}

/**
 * stop the pump
 */
int pumpStop() {
  if (isTestingPump) {
    return 0;
  }
  digitalWrite(GPIO_PUMP, LOW);
  digitalWrite(GPIO_LED, LOW);
  isPumping = 0;
  return 0;
}

/**
 * Tab virtual class.
 * Each tab should inherit this virtual class and implement the necessary
 * functions
 */
class Tab {
protected:
  /**
   * needRepaint
   * the variable that represent if we need a repaint. It should be
   * initialized by 1. If repaint() is called, it should be set to 1.
   */
  int needRepaint;
public:
  /**
   * constructor
   */
  Tab() : 
  needRepaint(1) {
  }

  /**
   * paint the UI on LCD.
   * In this function, you should only update the LCD when needRepaint is not
   * 0. And after LCD is updated, you should set needRepaint to 0 or call
   * this parent function.
   * @param lcd the LiquidCrystal_I2C object to draw
   */
  virtual void paint(LiquidCrystal_I2C &lcd) {
    needRepaint = 0;
  }

  /**
   * repaint
   * Mark this UI needs a repaint. It sets needRepaint to 1 to notify
   * that paint() should really redraw the LCD
   */
  virtual void repaint() {
    needRepaint = 1;
  }

  /**
   * keyTyped.
   * This funciton will be called when there's a key pressed event that
   * need to be handled.
   * @param key the key value (pin number of the key)
   */
  virtual void keyTyped(int key) {
  }
};

/**
 * The Tab showing status.
 *
 * This tab shows the status of the machine. This tab should
 * be the default Tab when the user is not operating the UI.
 */
class TabStat : 
public Tab {
private:
  int oldTimeSS;
  int old_workingMode;
public:
  TabStat() : 
  Tab(),
  oldTimeSS(-1),
  old_workingMode(-1) {
  }
  void paint(LiquidCrystal_I2C &lcd) {
    int timeSS;
    Ds1302::DateTime t1;
    rtc.getDateTime(&t1);
    timeSS = t1.second;
    if (oldTimeSS != timeSS) {
      repaint();
      oldTimeSS = timeSS;
    }
    if (old_workingMode != workingMode) {
      repaint();
      old_workingMode = workingMode;
    }    
    if (needRepaint != 0) {
      lcd.clear();
      lcd.setCursor(0,0);
      //lcd.print(rtc.getTimeStr());
      lcd.print((t1.hour / 10) % 10);
      lcd.setCursor(1,0);
      lcd.print((t1.hour) % 10);
      lcd.setCursor(2,0);
      lcd.print(':');
      lcd.setCursor(3,0);
      lcd.print((t1.minute / 10) % 10);
      lcd.setCursor(4,0);
      lcd.print((t1.minute) % 10);
      lcd.setCursor(5,0);
      lcd.print(':');
      lcd.setCursor(6,0);
      lcd.print((t1.second / 10) % 10);
      lcd.setCursor(7,0);
      lcd.print((t1.second) % 10);
      lcd.setCursor(11, 0);
      lcd.print(F("Sw:"));
      lcd.setCursor(14,0);
      lcd.print(waterSpeedLevel);
      lcd.setCursor(3, 1);
      if ( workingMode == 0 ) {
        lcd.print(F("No Setting"));
      } 
      else if (workingMode == 3) {
        lcd.print(F("C+M Mode"));
      } 
      else if (workingMode == 1) {
        lcd.print(F("Time Mode"));
      } 
      else if (workingMode == 2) {
        lcd.print(F("Hsoil Mode"));
      }
      lcd.noBlink();
    }
    Tab::paint(lcd);
  }
  void keyTyped(int key);
} 
tabStat;

/**
 * The pointer that points to the current active Tab
 */
class Tab *currentTab = &tabStat;

/**
 * The Tab shows the Main Menu. This is the Tab to show when user pressed
 * buttons from TabStat
 */
class TabMainMenu : 
public Tab {
private:
  int showTabMainMenuCursorLine;
  int showTabMainMenuStartLine;
public:
  static const char * const mainMenuItem[8];
  TabMainMenu() : 
  Tab(), showTabMainMenuCursorLine(0),
  showTabMainMenuStartLine(0)
  {
  }

  void paint(LiquidCrystal_I2C &lcd) {
    const int n = sizeof(mainMenuItem)/sizeof(mainMenuItem[0]);
    /* Check the value of the line cursor */
    if (showTabMainMenuCursorLine >= n) {
      showTabMainMenuCursorLine=n-1;
    }
    if (showTabMainMenuCursorLine < 0) {
      showTabMainMenuCursorLine=0;
    }
    /* Adjusting the start line to show */
    if (showTabMainMenuCursorLine < showTabMainMenuStartLine) {
      showTabMainMenuStartLine = showTabMainMenuCursorLine;
    }
    if (showTabMainMenuCursorLine > showTabMainMenuStartLine+1) {
      showTabMainMenuStartLine = showTabMainMenuCursorLine-1;
    }
    if (needRepaint != 0) {
      lcd.clear();
      for (int i=showTabMainMenuStartLine; i<showTabMainMenuStartLine+2 && i<n; i++) {
        lcd.setCursor(0,i-showTabMainMenuStartLine);
        lcd.print(FPSTR001(pgm_read_word(&mainMenuItem[i])));
      }
      lcd.setCursor(0,showTabMainMenuCursorLine-showTabMainMenuStartLine);
      lcd.blink();
    }
    Tab::paint(lcd);
  }
  void keyTyped(int key);
} 
tabMainMenu;

const char mainMenuItem001[] PROGMEM = {"1: Soil Humid"};
const char mainMenuItem002[] PROGMEM = {"2: Water Speed"};
const char mainMenuItem003[] PROGMEM = {"3: Water Time"};
const char mainMenuItem004[] PROGMEM = {"4: Water Days"};
const char mainMenuItem005[] PROGMEM = {"5: System Date"};
const char mainMenuItem006[] PROGMEM = {"6: System Time"};
const char mainMenuItem007[] PROGMEM = {"7: LCD BackLight"};
const char mainMenuItem008[] PROGMEM = {"8: WaterDuration"};

const char * const TabMainMenu::mainMenuItem[] PROGMEM = {
    mainMenuItem001,
    mainMenuItem002,
    mainMenuItem003,
    mainMenuItem004,
    mainMenuItem005,
    mainMenuItem006,
    mainMenuItem007,
    mainMenuItem008 };

const char password[] PROGMEM = {"\x05\x06\x07\x08"};
/**
 * The Tab for user to input the password
 */
class TabPassword : public Tab {
private:
  int currentPasswordPos;
  int checkFlag;
public:
  TabPassword() :
  Tab(), currentPasswordPos(0), checkFlag(0) {
  }

  void paint(LiquidCrystal_I2C &lcd) {
    unsigned long currentTime = millis();
    if ((currentPasswordPos != 0 || checkFlag != 0) &&
        (currentTime >= keyPressedTime + 10000
     || currentTime < keyPressedTime)) {
      currentPasswordPos = 0;
      checkFlag = 0;
      repaint();
    }
    if (needRepaint != 0) {
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print(F("Password?"));
      for (int i=0; i<currentPasswordPos; i++) {
        lcd.setCursor(i,1);
	lcd.print('*');
      }
      lcd.setCursor(currentPasswordPos, 1);
      lcd.blink();
    }
    Tab::paint(lcd);
  }
  void keyTyped(int key) {
    int N = strnlen_P(password, 8);
    if (currentPasswordPos >= N) {
      currentPasswordPos = 0;
      checkFlag = 0;
      repaint();
    }
    if (pgm_read_byte(&password[currentPasswordPos]) != key) {
      checkFlag = 1;
    }
    currentPasswordPos++;
    repaint();
    if (currentPasswordPos >= N) {
      currentPasswordPos = 0;
      if (checkFlag == 0) {
        checkFlag=0;
	lcd.noBlink();
        currentTab = &tabMainMenu;
        repaint();
      } else {
        checkFlag=0;
	repaint();
      }
    }
  }
} tabPassword;

void TabStat::keyTyped(int key) {
  /* If the user pressed button at PIN 5, let the TabMainMenu to be the
     active Tab */
  if (key == 5) {
    if (lockMode == 1) {
      currentTab = &tabPassword;
    } else {
      currentTab = &tabMainMenu;
    }
    repaint();
  }
}

class TabSetTime : 
public Tab {
private:
  int showTabSetTimeCursor;
  int timeHH;
  int timeMM;
  int timeSS;
  void setTime(int HH, int MM, int SS) {
    Ds1302::DateTime t;
    rtc.getDateTime(&t);
    t.hour = HH;
    t.minute = MM;
    t.second = SS;
    rtc.setDateTime(&t);
  }
public:
  TabSetTime() : 
  Tab(),
  showTabSetTimeCursor(0),
  timeHH(-1),
  timeMM(-1),
  timeSS(-1) {
  }
  void paint(LiquidCrystal_I2C &lcd) {
    char tmp1[10];
    if (timeHH < 0 || timeMM < 0 || timeSS < 0) {
      Ds1302::DateTime t;
      rtc.getDateTime(&t);
      timeHH = t.hour;
      timeMM = t.minute;
      timeSS = t.second;
      repaint();
    }
    if (needRepaint != 0) {
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print(F("Set System Time"));
      lcd.setCursor(0,1);
      snprintf_P(tmp1,sizeof(tmp1)-1,PSTR("%02d:%02d:%02d"),timeHH,timeMM,timeSS);
      lcd.print(tmp1);
      lcd.blink();
      lcd.setCursor(3*showTabSetTimeCursor+1,1);
    }
    Tab::paint(lcd);
  }
  void keyTyped(int key) {
    if (key == 8) {
      currentTab = &tabMainMenu;
      lcd.noBlink();
      repaint();
      showTabSetTimeCursor=0;
      setTime(timeHH, timeMM, timeSS);
      timeHH=-1;
      timeMM=-1;
      timeSS=-1;
    } 
    else if (key == 7) {
      switch(showTabSetTimeCursor) {
      case 0:
        timeHH = (timeHH+1)%24;
        break;
      case 1:
        timeMM = (timeMM+1)%60;
        break;
      case 2:
        timeSS = (timeSS+1)%60;
        break;
      default:
        showTabSetTimeCursor=0;
        break;
      }
      repaint();
    } 
    else if (key == 6) {
      switch(showTabSetTimeCursor) {
      case 0:
        timeHH = (timeHH+23)%24;
        break;
      case 1:
        timeMM = (timeMM+59)%60;
        break;
      case 2:
        timeSS = (timeSS+59)%60;
        break;
      default:
        showTabSetTimeCursor=0;
        break;
      }
      repaint();
    } 
    else if (key == 5) {
      showTabSetTimeCursor=(showTabSetTimeCursor+1)%3;
      repaint();
    }
  }
} 
tabSetTime;

class TabSetDate : 
public Tab {
private:
  int showTabSetDateCursor;
  int dateYY;
  int dateMM;
  int dateDD;
  void setDate(int DD, int MM, int YY) {
      Ds1302::DateTime t;
      rtc.getDateTime(&t);
      t.year = YY % 100;
      t.month = MM;
      t.day = DD;
      rtc.setDateTime(&t);
  }
public:
  static const int daysOfMonth[12] PROGMEM;
  TabSetDate() : 
  Tab(),
  showTabSetDateCursor(0),
  dateYY(-1),
  dateMM(-1),
  dateDD(-1)
  {
  }
  void paint(LiquidCrystal_I2C &lcd) {
    char tmp1[18];
    Ds1302::DateTime t;
    if (dateYY < 0 || dateMM < 0 || dateDD < 0) {
      rtc.getDateTime(&t);
      dateYY = t.year;
      dateMM = t.month;
      dateDD = t.day;
      repaint();
    }
    if (needRepaint != 0) {
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print(F("Set System Date"));
      lcd.setCursor(0,1);
      snprintf_P(tmp1,sizeof(tmp1)-1,PSTR("%04d-%02d-%02d"),dateYY,dateMM,dateDD);
      lcd.print(tmp1);
      lcd.setCursor(11,1);
      rtc.getDateTime(&t);
      switch(t.dow) {
      case Ds1302::DOW_MON:
        lcd.print(F("Mon"));
        break;
      case Ds1302::DOW_TUE:
        lcd.print(F("Tue"));
        break;
      case Ds1302::DOW_WED:
        lcd.print(F("Wed"));
        break;
      case Ds1302::DOW_THU:
        lcd.print(F("Thu"));
        break;
      case Ds1302::DOW_FRI:
        lcd.print(F("Fri"));
        break;
      case Ds1302::DOW_SAT:
        lcd.print(F("Sat"));
        break;
      case Ds1302::DOW_SUN:
        lcd.print(F("Sun"));
        break;
      default:
        lcd.print(F("   "));
        break;
      }
      lcd.blink();
      lcd.setCursor(2+3*showTabSetDateCursor+1,1);
    }
    Tab::paint(lcd);
  }
  void keyTyped(int key) {
    if (key == 8) {
      currentTab = &tabMainMenu;
      lcd.noBlink();
      showTabSetDateCursor=0;
      setDate(dateDD, dateMM, dateYY);
      dateYY=-1;
      dateMM=-1;
      dateDD=-1;
      repaint();
    } 
    else if (key == 7) {
      switch(showTabSetDateCursor) {
      case 0:
        if (dateYY < 2999) {
          dateYY = (dateYY+1);
          setDate(dateDD, dateMM, dateYY);
        }
        break;
      case 1:
        dateMM = ((dateMM-1)+1)%12+1;
        setDate(dateDD, dateMM, dateYY);
        break;
      case 2: 
        {
          int n1=0;
          if (dateMM >= 1 && dateMM-1 < sizeof(daysOfMonth)/sizeof(daysOfMonth[0])) {
            n1 = pgm_read_word(&daysOfMonth[dateMM-1]);
            if (dateYY % 400 == 0 || (dateYY%4 == 0 && dateYY % 100 != 0)) {
              if (dateMM==2) {
                n1++;
              }
            }
          }
          if (n1 == 0) {
            break;
          }
          dateDD = ((dateDD-1)+1)%n1+1;
          setDate(dateDD, dateMM, dateYY);
          break;
        }
      default:
        showTabSetDateCursor=0;
        break;
      }
      repaint();
    } 
    else if (key == 6) {
      switch(showTabSetDateCursor) {
      case 0:
        if (dateYY > 2000) {
          dateYY = dateYY-1;
          setDate(dateDD, dateMM, dateYY);
        }
        break;
      case 1:
        dateMM = ((dateMM-1)+11)%12+1;
        setDate(dateDD, dateMM, dateYY);
        break;
      case 2: 
        {
          int n1=0;
          if (dateMM >= 1 && dateMM-1 < sizeof(daysOfMonth)/sizeof(daysOfMonth[0])) {
            n1 = pgm_read_word(&daysOfMonth[dateMM-1]);
            if (dateYY % 400 == 0 || (dateYY%4 == 0 && dateYY % 100 != 0)) {
              if (dateMM==2) {
                n1++;
              }
            }
          }
          if (n1 == 0) {
            break;
          }
          dateDD = ((dateDD-1)+n1-1)%n1+1;
          setDate(dateDD, dateMM, dateYY);
          break;
        }
      default:
        showTabSetDateCursor=0;
        break;
      }
      repaint();
    } 
    else if (key == 5) {
      showTabSetDateCursor=(showTabSetDateCursor+1)%3;
      repaint();
    }
  }
} 
tabSetDate;

const int TabSetDate::daysOfMonth[] PROGMEM = { 
    31,28,31,30,31,30,31,31,30,31,30,31   };

class TabSoilHumid : 
public Tab {
private:
  int showTabSoilHumidCursor;
  const int configWorkingMode;
  const int configHumidTriggerLevel;

public:
  TabSoilHumid() : 
  Tab(),
  showTabSoilHumidCursor(0),
  configWorkingMode(4),
  configHumidTriggerLevel(3) {
  }
  void paint(LiquidCrystal_I2C &lcd) {
    if (needRepaint != 0) {
      lcd.clear();
      lcd.setCursor(1,0);
      lcd.print(F("Set Hsoil"));
      lcd.setCursor(14,0);
      lcd.print(humidTriggerLevel);
      lcd.setCursor(4,1);
      if (workingMode & 0x02) {
        lcd.print(F("OPEN"));
      } 
      else {
        lcd.print(F("CLOSE"));
      }
      switch(showTabSoilHumidCursor) {
      case 0:
        lcd.setCursor(4,1);
        break;
      case 1:
        lcd.setCursor(14,0);
        break;
      default:
        showTabSoilHumidCursor=0;
        break;
      }
      lcd.blink();
    }
    Tab::paint(lcd);
  }
  void keyTyped(int key) {
    if (key == 8) {
      currentTab = &tabMainMenu;
      lcd.noBlink();
      showTabSoilHumidCursor=0;
      memcpy_P(&configData_i, &(configData[configHumidTriggerLevel]),
               sizeof(struct configDataType));
      EEPROM.write(configData_i.eepromAddr, humidTriggerLevel);
      memcpy_P(&configData_i, &(configData[configWorkingMode]),
               sizeof(struct configDataType));
      EEPROM.write(configData_i.eepromAddr, workingMode);
      repaint();
    } 
    else if (key == 7) {
      switch(showTabSoilHumidCursor) {
      case 0:
        memcpy_P(&configData_i, &(configData[configWorkingMode]),
                 sizeof(struct configDataType));
        *((int *)(configData_i.data)) ^= 0x02;
        break;
      case 1:
        configDataAddOne(configHumidTriggerLevel);
        break;
      default:
        showTabSoilHumidCursor=0;
        break;
      }
      repaint();
    } 
    else if (key == 6) {
      switch(showTabSoilHumidCursor) {
      case 0:
        memcpy_P(&configData_i, &(configData[configWorkingMode]),
                 sizeof(struct configDataType));
        *((int *)(configData_i.data)) ^= 0x02;
        break;
      case 1:
        configDataSubtractOne(configHumidTriggerLevel);
        break;
      default:
        showTabSoilHumidCursor=0;
        break;
      }
      repaint();
    } 
    else if (key == 5) {
      showTabSoilHumidCursor=(showTabSoilHumidCursor+1)%2;
      repaint();
    }
  }
} 
tabSoilHumid;

class TabWaterSpeed : 
public Tab {
private:
  const int configWaterSpeedLevel;
public:
  TabWaterSpeed() : 
  Tab(), configWaterSpeedLevel(8) {
  }
  void paint(LiquidCrystal_I2C &lcd) {
    if (needRepaint != 0) {
      lcd.clear();
      lcd.setCursor(1,0);
      lcd.print(F("Set Swater"));
      lcd.setCursor(6,1);
      lcd.print(waterSpeedLevel);
      lcd.setCursor(6,1);
      lcd.blink();
    }
    Tab::paint(lcd);
  }
  void keyTyped(int key) {
    switch(key) {
    case 8:
      isTestingPump=0;
      pumpStop();
      currentTab = &tabMainMenu;
      lcd.noBlink();
      memcpy_P(&configData_i, &(configData[configWaterSpeedLevel]),
               sizeof(struct configDataType));
      EEPROM.write(configData_i.eepromAddr, waterSpeedLevel);
      repaint();
      break;
    case 7:
      configDataAddOne(configWaterSpeedLevel);
      repaint();
      break;
    case 6:
      configDataSubtractOne(configWaterSpeedLevel);
      repaint();
      break;
    case 5:
      isTestingPump=0;
      pumpStop();
      isTestingPump=1;
      pumpStart();
      break;
    default:
      break;
    }
  }
} 
tabWaterSpeed;

class TabWaterTime : 
public Tab {
private:
  int showTabWaterTimeNeedRefresh;
  int showTabWaterTimeCursor;
  const int configClockHH;
  const int configClockMM;
  const int configWorkingMode;
public:
  TabWaterTime() : 
  Tab(),
  showTabWaterTimeNeedRefresh(1),
  showTabWaterTimeCursor(0),
  configClockHH(1),
  configClockMM(2),
  configWorkingMode(4) {
  }
  void paint(LiquidCrystal_I2C &lcd) {
    if (needRepaint != 0) {
      char tmp1[10];
      lcd.clear();
      lcd.setCursor(4,0);
      snprintf_P(tmp1,sizeof(tmp1)-1, PSTR("%02d:%02d"), clockHH, clockMM);
      lcd.print(tmp1);
      lcd.setCursor(4,1);
      if (workingMode & 0x01) {
        lcd.print(F("OPEN"));
      } 
      else {
        lcd.print(F("CLOSE"));
      }
      switch(showTabWaterTimeCursor) {
      case 0:
        lcd.setCursor(4,1);
        break;
      case 1:
        lcd.setCursor(4,0);
        break;
      case 2:
        lcd.setCursor(7,0);
        break;
      default:
        showTabWaterTimeCursor=0;
        break;
      }
      lcd.blink();
    }
    Tab::paint(lcd);
  }
  void keyTyped(int key) {
    if (key == 8) {
      currentTab = &tabMainMenu;
      lcd.noBlink();
      showTabWaterTimeCursor=0;
      memcpy_P(&configData_i, &(configData[configClockHH]),
               sizeof(struct configDataType));
      EEPROM.write(configData_i.eepromAddr, clockHH);
      memcpy_P(&configData_i, &(configData[configClockMM]),
               sizeof(struct configDataType));
      EEPROM.write(configData_i.eepromAddr, clockMM);
      memcpy_P(&configData_i, &(configData[configWorkingMode]),
               sizeof(struct configDataType));
      EEPROM.write(configData_i.eepromAddr, workingMode);
      repaint();
    } 
    else if (key == 7) {
      switch(showTabWaterTimeCursor) {
      case 0:
        memcpy_P(&configData_i, &(configData[configWorkingMode]),
                 sizeof(struct configDataType));
        *((int *)(configData_i.data)) ^= 0x01;
        break;
      case 1:
        configDataAddOne(configClockHH);
        break;
      case 2:
        configDataAddOne(configClockMM);
        break;
      default:
        showTabWaterTimeCursor=0;
        break;
      }
      repaint();
    } 
    else if (key == 6) {
      switch(showTabWaterTimeCursor) {
      case 0:
        memcpy_P(&configData_i, &(configData[configWorkingMode]),
                 sizeof(struct configDataType));
        *((int *)(configData_i.data)) ^= 0x01;
        break;
      case 1:
        configDataSubtractOne(configClockHH);
        break;
      case 2:
        configDataSubtractOne(configClockMM);
        break;
      default:
        showTabWaterTimeCursor=0;
        break;
      }
      repaint();
    } 
    else if (key == 5) {
      showTabWaterTimeCursor=(showTabWaterTimeCursor+1)%3;
      repaint();
    }
  }
} 
tabWaterTime;

class TabWaterDays : 
public Tab {
private:
  const int configClockDD;
public:
  TabWaterDays() : 
  Tab(), configClockDD(0) {
  }
  void paint(LiquidCrystal_I2C &lcd) {
    if (needRepaint != 0) {
      char tmp1[5];
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print(F("Days Interval"));
      lcd.setCursor(0,1);
      lcd.print(F("Each "));
      snprintf_P(tmp1,sizeof(tmp1)-1, PSTR("%2d"), clockDD);
      lcd.print(tmp1);
      lcd.print(F(" Days"));
      lcd.setCursor(6,1);
      lcd.setCursor(6,1);
      lcd.blink();
    }
    Tab::paint(lcd);
  }
  void keyTyped(int key) {
    if (key == 8) {
      currentTab = &tabMainMenu;
      lcd.noBlink();
      memcpy_P(&configData_i, &(configData[configClockDD]),
               sizeof(struct configDataType));
      EEPROM.write(configData_i.eepromAddr, clockDD);
      repaint();
    } 
    else if (key == 7) {
      configDataAddOne(configClockDD);
      repaint();
    } 
    else if (key == 6) {
      configDataSubtractOne(configClockDD);
      repaint();
    } 
    else if (key == 5) {
    }
  }
} 
tabWaterDays;

class TabLCDBacklight : 
public Tab {
private:
  const int configLCDBacklightLevel;
public:
  TabLCDBacklight() : 
  Tab(), configLCDBacklightLevel(6) {
  }
  void paint(LiquidCrystal_I2C &lcd) {
    if (needRepaint != 0) {
      lcd.clear();
      lcd.setCursor(1,0);
      lcd.print(F("Set LCDLight"));
      lcd.setCursor(6,1);
      lcd.print(lcdBacklightLevel);
      lcd.setCursor(6,1);
      lcd.blink();
    }
    Tab::paint(lcd);
  }
  void keyTyped(int key) {
    if (key == 8) {
      currentTab = &tabMainMenu;
      lcd.noBlink();
      memcpy_P(&configData_i, &(configData[configLCDBacklightLevel]),
               sizeof(struct configDataType));
      EEPROM.write(configData_i.eepromAddr, lcdBacklightLevel);
      repaint();
    } 
    else if (key == 7) {
      configDataAddOne(configLCDBacklightLevel);
      repaint();
    } 
    else if (key == 6) {
      configDataSubtractOne(configLCDBacklightLevel);
      repaint();
    } 
    else if (key == 5) {
    }
  }
} 
tabLCDBacklight;

class TabWaterDuration : 
public Tab {
private:
  const int configWaterDurationSec;
public:
  TabWaterDuration() : 
  Tab(), configWaterDurationSec(7) {
  }
  void paint(LiquidCrystal_I2C &lcd) {
    if (needRepaint != 0) {
      lcd.clear();
      lcd.setCursor(1,0);
      lcd.print(F("Set water time"));
      lcd.setCursor(7,1);
      lcd.print(waterDurationSec);
      lcd.setCursor(7,1);
      lcd.blink();
    }
    Tab::paint(lcd);
  }
  void keyTyped(int key) {
    if (key == 8) {
      currentTab = &tabMainMenu;
      lcd.noBlink();
      memcpy_P(&configData_i, &(configData[configWaterDurationSec]),
               sizeof(struct configDataType));
      EEPROM.write(configData_i.eepromAddr, waterDurationSec);
      repaint();
    } 
    else if (key == 7) {
      configDataAddOne(configWaterDurationSec);
      repaint();
    } 
    else if (key == 6) {
      configDataSubtractOne(configWaterDurationSec);
      repaint();
    } 
    else if (key == 5) {
    }
  }
} 
tabWaterDuration;

void TabMainMenu::keyTyped(int key) {
  const int n = sizeof(mainMenuItem)/sizeof(mainMenuItem[0]);
  if (key == 8) {
    currentTab = &tabStat;
    lcd.noBlink();
    showTabMainMenuCursorLine=0;
    showTabMainMenuStartLine=0;
    repaint();
  } 
  else if (key == 7) {
    if (showTabMainMenuCursorLine+1 < n) {
      showTabMainMenuCursorLine ++;
    }
    repaint();
  } 
  else if (key == 6) {
    if (showTabMainMenuCursorLine > 0) {
      showTabMainMenuCursorLine --;
    }
    repaint();
  } 
  else if (key == 5) {
    switch(showTabMainMenuCursorLine) {
    case 0:
      currentTab = &tabSoilHumid;
      break;
    case 1:
      currentTab = &tabWaterSpeed;
      break;
    case 2:
      currentTab = &tabWaterTime;
      break;
    case 3:
      currentTab = &tabWaterDays;
      break;
    case 4:
      currentTab = &tabSetDate;
      break;
    case 5:
      currentTab = &tabSetTime;
      break;
    case 6:
      currentTab = &tabLCDBacklight;
      break;
    case 7:
      currentTab = &tabWaterDuration;
      break;
    default:
      break;
    }
    repaint();
  }
}

/**
 * show - display the tab
 * this function will call currentTab's paint() and keyTyped().
 */
void show() {
  currentTab->paint(lcd);
  if ( ! keyQueueIsEmpty() ) {
    byte key1;
    key1 = keyQueuePoll();
    currentTab->keyTyped(key1);
  }
}

/**
 * turn off the LCD backlight if key is not pressed for long time.
 */
void controlLCDBacklight() {
  int set_lcd;
  unsigned long currentTime = millis();
  set_lcd = 255*((lcdBacklightLevel-1)%4)/3;
  if (currentTime < keyPressedTime + 10000) {
    /* We turn on the backlight if there's key activity in 10 sec */
    analogWrite(GPIO_LCDBACKLIGHT, set_lcd);
  } 
  else if (currentTab != &tabStat) {
    /* We turn on the backlight if the current TAB isn't tabStat */
    if (currentTime < keyPressedTime + 60000) {
      analogWrite(GPIO_LCDBACKLIGHT, set_lcd);
    } 
    else {
      /* However, if it stays too long, we will set the TAB to tabStat */
      currentTab->repaint();
      currentTab = &tabStat;
      currentTab->repaint();
    }
  } 
  else {
    digitalWrite(GPIO_LCDBACKLIGHT, LOW);
  }
}

/**
 * watering the flower based on RTC clock
 */
void waterByClock() {
  if (workingMode & 0x01) {
    /* only works when the machine is set to Time Mode */
    Ds1302::DateTime t;
    long t1;
    long tStart;
    long tEnd;
    
    rtc.getDateTime(&t);
    /* calculate current seconds of the day */
    t1 = ((long)(t.hour))*60L*60L+((long)(t.minute))*60L+((long)(t.second));
    /* the start second of the day */
    tStart = ((long)(clockHH))*60L*60L+((long)(clockMM))*60L;
    /* the end second of the day */
    tEnd = ((long)(clockHH))*60L*60L+((long)(clockMM))*60L+((long)(waterDurationSec));
    /* If the tEnd across the day, we might need also adjust t1 to across
     the day properly by adding 86400 to t1 */
    if (tEnd >= 60L*60L*24L) {
      if (t1 < tStart && tStart <= (t1+60L*60L*24L) && (t1+60L*60L*24L) <= tEnd) {
        t1 = t1 + 60L*60L*24L;
      }
    }
    if (clockDD == 1 || ((t.day % clockDD) == 1)) {
      if ( tStart <= t1 && t1 <= tEnd) {
        pumpStart();
      } 
      else {
        pumpStop();
      }
    } 
    else {
      pumpStop();
    }
  }
  if (workingMode == 0) {
    pumpStop();
  }
}

/**
 * This funciton maps trigger level to actual sensor value.
 *
 * @humidTriggerLevel the trigger level, should be 1~3
 * @return the value of sensor
 */
int getHumidTriggerByLevel(int humidTriggerLevel) {
  int set_hsoil=0;
  switch(humidTriggerLevel) {
  case 1:
    set_hsoil=550;
    break;
  case 2:
    set_hsoil=650;
    break;
  case 3:
    set_hsoil=750;
    break;
  default:
    break;
  }
  return set_hsoil;
}

/**
 * water by Humid
 */
void waterByHumid() {
  if (workingMode & 0x02) {
    if (analogRead(GPIO_SOILHUMID) > getHumidTriggerByLevel(humidTriggerLevel)) {
      pumpStart();
    } 
    else {
      pumpStop();
    }
  }
  if (workingMode == 0) {
    pumpStop();
  }
}

void loop()
{
  unsigned long currentTime_start = millis();
  unsigned long currentTime_end = 0;
  wdt_reset();  /* pet watchdog to avoid resetting */
  waterByClock();
  waterByHumid();
  keyScan();
  controlLCDBacklight();
  show();
  handleSerialCommand();
  currentTime_end = millis();
  /* 10 fps should be good enough. Let CPU delay and sleep. */
  if (currentTime_end >= currentTime_start && currentTime_end - currentTime_start < 100) {
    delay(100 - (currentTime_end - currentTime_start));
  }
}
