# Kochin auto flower watering firmware

## Libraries

In order to build this firmware, some libraries that must be installed.

* LiquidCrytal_I2C - https://github.com/lucasmaziero/LiquidCrystal_I2C
  - The library to drive the LCD
* DS1302 - http://www.rinkydinkelectronics.com/library.php?id=5
  - The library to drive DS1302 RTC module
* MemoryFree - https://github.com/maniacbug/MemoryFree
  - The library to print out the free memory.
  - Not really a necessary one for real usage, but just for debugging.

## Setup

There is a variable PUMP_MAX_V which limits the maximum voltage that the pump
uses. We currently give L298N 12V input and the pump is R385 pump. So we can
safely give PUMP_MAX_V to 12 here. Please be aware that some pump are 5V and
you must change this variable to 5. Otherwise it burns your pump.

## UI and Buttons

One can think that the buttons, from left to right, are
"SELECT", "+", "-", "RETURN"

The Main menu can be bring up by pressing "SELECT".
And the menu can be scrolled by "+" and "-". And press "SELECT" to enter the
submenu.

After entering the submenu, you might be able to use "SELECT" to jump between
different fields. And use "+" or "-" to increase/decrease the values.
Long press of "+" and "-" can increase/decrease the value faster.

When you've set the config values corretly, press "RETURN" to save the value.
