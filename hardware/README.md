# Hardware

## Electronics

### BOM

* DS1302 - RTC clock
* L298N - Motor driver
* LCD1602 - LCD 16x2 with backlight
* Membrane 1x4 Keypad
* R385 Water Pump
* Soil Moisture Sensor YL-69/HL-69

### DS1302 RTC clock

This clock is cheap. Thus it is not so accurate. You might need to adjust its
time before you leave for your trip. For a week it might shift 30 minutes.

When power is connected, it is able to change the batteries without losing
the time.

### Membrane 1x4 Keypad

This keypad is really easy to be broken due to the humid environment.
Some keys will not function after half year. The worser case is the keypad
starts randomly pressed automatically and might change your configuration
through the UI by accident. We've designed a way to protect this case by
enabling the password. Users need to input a password to enter the main menu.
Default password is "1234". The password is written in code and
cannot be changed dynamically. To enable password protection we must go to
serial console and execute "setenv lockMode 1".

### Soil Moisture Sensor YL-69/HL-69

I'd suggest to not using YL-69.
Because the probe of YL-69 will anodic dissolution.
Please see this video. https://youtu.be/udmJyncDvw0

If you really want to detect the Moisture, we suggest that you can
try to replace the probe with 2 carbon stick (or pencil lead) with similar
length and let the two stick have similar gap. The original probe is easily
anodic dissolution because it is a thin layer of metal. But for carbon stick
it can last for many many years.

### Connect from Modules to Arduino Sensor Shield v5.0

| Module Name      | Module PIN | Arduino Sensor Shield v5.0 PIN  |
|------------------|------------|---------------------------------|
| DS1302           | CLK        | D4                              |
|                  | DAT        | D3                              |
|                  | RST        | D2                              |
| L298N            | ENB        | D9                              |
|                  | IN3        | VCC                             |
|                  | IN4        | GND                             |
| LCD1602          | SCL        | SCL (IIC)                       |
|                  | SDA        | SDA (IIC)                       |
| Moisture Sensor  | A0         | A0                              |
| 1x4 Keypad       | PIN1       | GND                             |
|                  | PIN2       | D6                              |
|                  | PIN3       | D5                              |
|                  | PIN4       | D8                              |
|                  | PIN5       | D7                              |

![connection](docs/img/components_connect.svg.png)
